package com.spring_JWT.utils;

import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @Author Goddy
 * @Date Create in 上午12:22 2018/1/11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JWTUtilTest {

    @Test
    public void createTest() throws Exception {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Jesslyn");
        jsonObject.put("ttlMillis", "200");
        jsonObject.put("uid", "2314912");
        String token = JWTUtil.createJWT("jwt", jsonObject.toString(), 600);

        System.out.println(token);
    }

    @Test
    public void parseTest() throws Exception {

        Claims claims = JWTUtil.parseJWT("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJqd3QiLCJzdWIiOiJ7XCJ1aWRcIjpcIjIzMTQ5MTJcIixcIm5hbWVcIjpcIkplc3NseW5cIixcInR0bE1pbGxpc1wiOlwiMjAwXCJ9IiwiaXNzIjoiR29kZHkiLCJpYXQiOjE1MTU2MDMzNDgsImV4cCI6MTUxNTYwMzM0OH0.GG4-SwW-Evd4JCWMqS3shQgTlQFaptP-5FAZ1v4dcyQ");
        System.out.println("u");
    }

}