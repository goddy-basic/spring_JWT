package com.spring_JWT;

import com.spring_JWT.annotation.MyLogAnnotation;
import com.spring_JWT.domain.converter.User2UserFullConverter;
import com.spring_JWT.domain.entity.UserFull;
import com.spring_JWT.domain.table.User;
import com.spring_JWT.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringJWTApplicationTests {

	@Autowired
    private RedisTemplate redisTemplate;

	@Autowired
    private UserService userService;

	@Test
    public void test() throws Exception {
		String uid = "1515576924004636875";
	    User user = userService.findUser(uid);
        ValueOperations<String, UserFull> operations = redisTemplate.opsForValue();

		UserFull userFull = User2UserFullConverter.convert(user);
		userFull.setJwt("12222222");
		userFull.setRoleList(userService.findRoles(uid));
		userFull.setPermissionList(userService.findPermissions(uid));

		operations.set("key1", userFull);

        Thread.sleep(1000);
        UserFull user1 = operations.get("key1");
        Assert.assertEquals("Jesslyn", user1.getUsername());
    }

	@Test
	public void contextLoads() {
	}

	@Test
//	@MyLogAnnotation(value = "你好")
	public void annotationTest() {
		ValueOperations<String, UserFull> operations = redisTemplate.opsForValue();
		Object object = operations.get("key1");
		System.out.println(object);

	}

}
