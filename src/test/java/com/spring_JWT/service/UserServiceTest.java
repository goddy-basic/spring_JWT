package com.spring_JWT.service;

import com.spring_JWT.domain.table.User;
import com.spring_JWT.utils.KeyUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.junit.Assert.*;

/**
 * @Author Goddy
 * @Date Create in 下午5:28 2018/1/10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void saveUser() throws Exception {
        User user = new User();
        user.setUid(KeyUtil.getUniKey());
        user.setUsername("Goddy");
        user.setPassword("123");
        User result = userService.saveUser(user);
        Assert.assertNotNull(result);
    }

    @Test
    public void deleteUser() throws Exception {
        userService.deleteUser("1515577906091524866");
    }

    @Test
    public void updateUser() throws Exception {
        User user = userService.findUser("1515576924004636875");
        user.setPassword("1234");
        User result = userService.updateUser(user);
        Assert.assertNotNull(result);
    }

    @Test
    public void addRoles() throws Exception {
        userService.addRoles("1515576924004636875",
                "admin");
    }

    @Test
    public void deleteRoles() throws Exception {
        userService.deleteRoles("1515576924004636875", "menu:add");
    }

    @Test
    public void findRoles() throws Exception {
        Set<String> result = userService.findRoles("1515576924004636875");
        Assert.assertNotEquals(0, result.size());
    }

    @Test
    public void addPermissions() throws Exception {
        userService.addPermissions("1515576924004636875",
                "menu:update", "menu:add");
    }

    @Test
    public void deletePermissions() throws Exception {
        userService.deletePermissions("1515576924004636875",
                "menu:add");
    }

    @Test
    public void findPermissions() throws Exception {
        Set<String> result = userService.findPermissions("1515576924004636875");
        Assert.assertEquals(2, result.size());
    }

}