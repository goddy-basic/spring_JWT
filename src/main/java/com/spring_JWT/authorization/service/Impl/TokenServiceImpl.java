package com.spring_JWT.authorization.service.Impl;

import com.spring_JWT.authorization.object.Token;
import com.spring_JWT.authorization.service.TokenService;

/**
 * 通过 Redis 存储和验证 token 的实现类
 * @Author Goddy
 * @Date Create in 下午7:02 2018/1/9
 */
public class TokenServiceImpl implements TokenService {

    @Override
    public Token createToken(Long userId) {
        return null;
    }

    @Override
    public boolean checkToken(Token token) {
        return false;
    }

    @Override
    public Token getToken(String authentication) {
        return null;
    }

    @Override
    public void deleteToken(Long userId) {

    }
}
