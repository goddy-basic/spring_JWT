package com.spring_JWT.authorization.service;

import com.spring_JWT.authorization.object.Token;

/**
 * 对token进行操作的接口
 * @Author Goddy
 * @Date Create in 下午6:51 2018/1/9
 */
public interface TokenService {

    /**
     * 创建一个token关联上的指定用户
     * @param userId 指定用户的id
     * @return 生成的token
     */
    Token createToken(Long userId);

    /**
     * 检查token是否有效
     * @param token token对象
     * @return 是否有效
     */
    boolean checkToken(Token token);

    /**
     * 从字符串中解析token
     * @param authentication 加密后的字符串
     * @return
     */
    Token getToken(String authentication);

    void deleteToken(Long userId);

}
