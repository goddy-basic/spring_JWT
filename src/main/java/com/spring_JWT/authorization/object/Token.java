package com.spring_JWT.authorization.object;

import lombok.Data;

/**
 * Token 的 Model 类，可以增加字段提高安全性，例如时间戳、url 签名
 * @Author Goddy
 * @Date Create in 下午6:51 2018/1/9
 */
@Data
public class Token {

    /** 用户id */
    private Long userId;

    /** 随机生成的uid */
    private String token;

    public Token(Long userId, String token) {
        this.userId = userId;
        this.token = token;
    }
}
