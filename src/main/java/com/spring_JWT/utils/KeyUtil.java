package com.spring_JWT.utils;

import java.util.Random;

/**
 * @Author Goddy
 * @Date Create in 下午3:24 2018/1/10
 */
public class KeyUtil {

    /**
     * 生成唯一的主键
     * 格式：时间+随机数
     * synchronized 防止多线程
     * @return
     */
    public static synchronized String getUniKey() {
        Random random = new Random();

        System.currentTimeMillis();

        Integer number = random.nextInt(900000) + 100000;

        return System.currentTimeMillis() + String.valueOf(number);
    }
}
