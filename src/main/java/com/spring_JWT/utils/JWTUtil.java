package com.spring_JWT.utils;

import com.spring_JWT.enums.Constant;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import io.jsonwebtoken.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

/**
 * @Author Goddy
 * @Date Create in 下午8:21 2018/1/10
 */
public class JWTUtil {

    @Value("${JWT.key}")
    private static String ymlKey;

    /**
     * 创建jwt
     * @param id JWT_ID
     * @param subject 主题,用户信息的json字符串
     * @param ttlMillis 过期的秒数,token的有效期，时间较短，需要定时更新
     * @return JWT
     */
    public static String createJWT(String id, String subject, long ttlMillis) throws Exception {

        //由字符串生成加密key
        SecretKey key = generalKey();
        //获取当前日期的时间戳及日期
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        //算法
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        JwtBuilder builder = Jwts.builder()
                .setId(id)      //JWT_ID
//                .setAudience("")        //接受者
//                .setClaims(null)        //自定义属性
                .setSubject(subject)     //主题
                .setIssuer("Goddy")     //签发者
                .setIssuedAt(now)       //签发时间
//                .setNotBefore(new Date())       //失效时间,直至才能用
//                .setExpiration(ttlMillis)      //过期时间
                .signWith(signatureAlgorithm, key);     //签名算法以及密匙

        if (ttlMillis > 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            System.out.println(nowMillis);
            System.out.println(now);
            System.out.println(expMillis);
            System.out.println(exp);
            builder.setExpiration(exp);     //过期时间
        }
        return builder.compact();
    }

    /**
     * 解密jwt
     * @param jwt
     * @return TODO 不知道
     */
    public static Claims parseJWT(String jwt) throws MyException {

        try {

            SecretKey key = generalKey();
            Claims claims = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(jwt)
                    .getBody();
            return claims;

        } catch (Exception e) {

            if (e instanceof  ExpiredJwtException) {        //token过期
                throw new MyException(ResultEnum.TOKEN_EXPIRED_ERROR);
            } else if (e instanceof SignatureException) {       //
                throw new MyException(ResultEnum.TOKEN_SIGNATURE_ERROR);
            } else {
                throw new MyException(ResultEnum.TOKEN_OTHER_ERROR);
            }
        }

    }

    /** 由字符串生成加密key */
    protected static SecretKey generalKey() {
        String stringKey = ymlKey + Constant.JWT_SECRET;
        byte[] encodedKey = Base64.decodeBase64(stringKey);
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }

}
