package com.spring_JWT.controller;

import com.spring_JWT.annotation.PermissionAnnotation;
import com.spring_JWT.domain.entity.Result;
import com.spring_JWT.service.UserService;
import com.spring_JWT.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午6:16 2018/1/11
 */
@RestController
@RequestMapping(value = "/p")
public class PermissionTestController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/test1")
    @PermissionAnnotation(permission = {"menu:update", "menu:add"})
    public Object test1() {
        return "finish";
    }

    @GetMapping(value = "/test2")
    @PermissionAnnotation(permission = {"menu:update"})
    public Object test2() {
        return "finish";
    }

    @GetMapping(value = "/test3")
    @PermissionAnnotation(permission = {"menu:update", "menu:delete"})
    public Object test3() {
        return "finish";
    }

    @GetMapping(value = "/findP")
    @Cacheable(cacheNames = "permission", key = "123")
    public Result findP() {
        Set<String> result = userService.findPermissions("1515576924004636875");
        return ResultUtil.success(result);
    }

//    @GetMapping(value = "/addP")
//    public Object addP() {
//
//    }

}
