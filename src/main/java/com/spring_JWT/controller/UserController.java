package com.spring_JWT.controller;

import com.spring_JWT.annotation.RoleAnnotation;
import com.spring_JWT.domain.entity.Result;
import com.spring_JWT.domain.table.User;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import com.spring_JWT.service.RedisService;
import com.spring_JWT.service.UserService;
import com.spring_JWT.utils.JWTUtil;
import com.spring_JWT.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @Author Goddy
 * @Date Create in 下午5:26 2018/1/10
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    /**
     * 登录 TODO 改成post
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @RequestMapping(value = "/login")
    public Result login(@RequestParam String username, @RequestParam String password) throws Exception {

        //1。判断数据库是否存在
        User user = userService.findUserByName(username);
        if (user == null) {
            throw new MyException(ResultEnum.DATABASE_NO_USER_ERROR);
        }

        //2。判断密码是否正确
        if (!user.getPassword().equals(password)) {
            throw new MyException(ResultEnum.USER_WRONG_PASSWORD_ERROR);
        }

        //3。生成jwt并存储
        String jwt = redisService.save(user, 300 * 1000L);

        //4。返回JWT
        return ResultUtil.success(jwt);

    }

    /**
     * 登出页面
     * @param headers http请求头部
     * @return
     */
    @RequestMapping(value = "/logout")
    public Result logout(@RequestHeader HttpHeaders headers) {

        if (headers.containsKey("token")) {
            redisService.delete(headers.get("token").get(0));
        }
        return ResultUtil.success();
    }

    /**
     * 测试
     * @return
     */
    @RoleAnnotation(role = "admin")
    @RequestMapping(value = "/test1")
    public Result test1() {
        return ResultUtil.success();
    }

    @RoleAnnotation(role = "guest")
    @RequestMapping(value = "/test2")
    public Result test2() {
        return ResultUtil.success();
    }

}
