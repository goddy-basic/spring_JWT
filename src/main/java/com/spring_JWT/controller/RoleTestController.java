package com.spring_JWT.controller;

import com.spring_JWT.annotation.MyLogAnnotation;
import com.spring_JWT.annotation.RoleAnnotation;
import com.spring_JWT.utils.JWTUtil;
import io.jsonwebtoken.Claims;
import net.sf.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

/**
 * @Author Goddy
 * @Date Create in 上午10:35 2018/1/10
 */
@RestController
@RequestMapping(value = "/r")
public class RoleTestController {

    @GetMapping(value = "hello")
    @MyLogAnnotation(value = "测试")
    public Object test(@RequestParam String name) {
        return "hello";
    }

    @GetMapping(value = "role")
    @RoleAnnotation(role = "user")
    public Object roleTest() {
        return "hello";
    }

    @GetMapping(value = "ann")
    @RoleAnnotation(role = "admin")
    public Object annTest() {
        return "finish";
    }

    @GetMapping(value = "create")
    public String createJWT() throws Exception {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Jesslyn");
        jsonObject.put("ttlMillis", "200");
        jsonObject.put("uid", "1515576924004636875");
        long exp = 30 * 1000;
        String str = jsonObject.toString();

        String token = JWTUtil.createJWT("myid", str, exp);
        return token;
    }

    @GetMapping(value = "parse")
    public Object parseJWT(@RequestHeader HttpHeaders headers) throws Exception {

        return "finish";
    }

}
