package com.spring_JWT.controller;

import com.spring_JWT.domain.entity.UserFull;
import com.spring_JWT.service.RedisService;
import com.spring_JWT.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Goddy
 * @Date Create in 上午11:07 2018/1/12
 */
@RestController
@RequestMapping(value = "/r")
public class RedisTestController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private UserService userService;

    //TODO 改成 post 请求
    @GetMapping("/login")
    public void login(@RequestParam String name, @RequestParam String password) {

        redisService.save(name, password);
    }

    @GetMapping("/find")
    public Object find(@RequestParam String key) {

        return redisService.findOne(key);
    }

    @GetMapping("/delete")
    public void delete(@RequestParam String key) {
        redisService.delete(key);
    }

}
