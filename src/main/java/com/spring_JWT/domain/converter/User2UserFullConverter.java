package com.spring_JWT.domain.converter;

import com.spring_JWT.domain.entity.UserFull;
import com.spring_JWT.domain.table.User;
import com.spring_JWT.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author Goddy
 * @Date Create in 下午5:39 2018/1/12
 */
public class User2UserFullConverter {

    public static UserFull convert(User user) {

        UserFull userFull = new UserFull();
        BeanUtils.copyProperties(user, userFull);
        return userFull;
    }

    public static List<UserFull> convert(List<User> userList) {
        return userList.stream().map(e ->
            convert(e)
        ).collect(Collectors.toList());
    }

}
