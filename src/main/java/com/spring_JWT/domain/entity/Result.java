package com.spring_JWT.domain.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author Goddy
 * @Date Create in 下午4:02 2018/1/10
 */
@Data
public class Result<T> implements Serializable {

    /**
     * 实现序列化
     * 快捷键 control + shift + i
     */
    private static final long serialVersionUID = 8665996118762061755L;

    /** 错误码 */
    private Integer code;

    /** 提示信息 */
    private String msg;

    /** 具体内容 */
    private T data;
}
