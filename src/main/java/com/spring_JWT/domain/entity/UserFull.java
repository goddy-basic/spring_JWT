package com.spring_JWT.domain.entity;

import lombok.Data;

import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午3:31 2018/1/10
 */
@Data
public class UserFull {

    /** 用户id */
    private String uid;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /** json web token */
    private String jwt;

    /** 用户角色 */
    private Set<String> roleList = null;

    /** 用户权限 */
    private Set<String> permissionList = null;

}
