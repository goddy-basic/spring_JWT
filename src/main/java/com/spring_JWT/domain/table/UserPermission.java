package com.spring_JWT.domain.table;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author Goddy
 * @Date Create in 下午3:10 2018/1/10
 */
@Entity
@Data
@Table(name = "ss_user_permission")
public class UserPermission {

    @Id @GeneratedValue
    private Long id;

    /** 用户id */
    private String uid;

    /** TODO 权限 以后改成权限id **/
    private String permission;

}
