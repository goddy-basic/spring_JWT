package com.spring_JWT.domain.table;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午6:34 2018/1/9
 */
@Entity
@Data
@Table(name = "ss_user")
public class User {

    /** 用户id */
    @Id @NotEmpty(message = "🆔ID必传")
    private String uid;

    /** 用户名 */
    @Column(unique = true)
    private String username;

    /** 密码 */
    private String password;

}
