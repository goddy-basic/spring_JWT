package com.spring_JWT.domain.table;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author Goddy
 * @Date Create in 下午3:12 2018/1/10
 */
@Entity
@Table(name = "ss_user_role")
@Data
public class UserRole {

    @Id @GeneratedValue
    private Long id;

    /** 用户id */
    private String uid;

    /** TODO 角色 以后改成角色id  */
    private String role;

}
