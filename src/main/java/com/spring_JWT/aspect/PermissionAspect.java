package com.spring_JWT.aspect;

import com.spring_JWT.annotation.PermissionAnnotation;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import com.spring_JWT.service.UserService;
import com.spring_JWT.utils.JWTUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午10:51 2018/1/11
 */
@Aspect
@Component
@Slf4j
public class PermissionAspect {

    @Autowired
    private UserService userService;

    @Pointcut("@annotation(com.spring_JWT.annotation.PermissionAnnotation)")
    public void permissionPointCut() {

    }

    @Before("permissionPointCut()")
    public void beforePermissionPointCut(JoinPoint joinPoint) {

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        PermissionAnnotation annotation = methodSignature.getMethod().getAnnotation(PermissionAnnotation.class);

        String[] permissions = annotation.permission();

        //获取headers
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String token = request.getHeader("token");

        Claims claims = JWTUtil.parseJWT(token);
        log.info(claims.toString());

        String subject = claims.getSubject();
        JSONObject body = JSONObject.fromObject(subject);
        String name = body.get("name").toString();
        String uid = body.get("uid").toString();

        //TODO 需要放到redis里
        Set<String> permissionSet = userService.findPermissions(uid);
        for (String permission : permissions) {
            log.info("【需要的权限】{}", permission);
            if (!permissionSet.contains(permission)) {
                throw new MyException(ResultEnum.UNAUTHORIZED_ERROR);
            }
        }

    }

}
