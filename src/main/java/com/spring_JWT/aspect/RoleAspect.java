package com.spring_JWT.aspect;

import com.spring_JWT.annotation.RoleAnnotation;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import com.spring_JWT.service.RedisService;
import com.spring_JWT.service.UserService;
import com.spring_JWT.utils.JWTUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections.map.ListOrderedMap;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午6:02 2018/1/10
 */
@Aspect
@Component
@Slf4j
public class RoleAspect {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @Pointcut("@annotation(com.spring_JWT.annotation.RoleAnnotation)")
    public void rolePointCut() {

    }

    @Before("rolePointCut()")
    public void beforeRolePointCut(JoinPoint joinPoint) {

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        RoleAnnotation annotation = methodSignature.getMethod().getAnnotation(RoleAnnotation.class);

        log.info("【接口需要的角色】{}", annotation.role());

        //获取headers
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String token = request.getHeader("token");

        //从redis根据token拿出数据
        JSONObject object = JSONObject.fromObject(redisService.findOne(token));
        JSONArray roles = JSONArray.fromObject(object.get("role"));
        log.info(object.toString());

        Claims claims = JWTUtil.parseJWT(token);
        log.info(claims.toString());

        if (!roles.contains(annotation.role())) {
            throw new MyException(ResultEnum.UNAUTHORIZED_ERROR);
        }

    }

}
