package com.spring_JWT.aspect;

import com.spring_JWT.annotation.MyLogAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Author Goddy
 * @Date Create in 下午9:00 2018/1/9
 */
@Aspect
@Component
@Slf4j
public class MyLogAspect {

    /**
     * @within(com.cxh.study.aop.controller.UserAccessAnnotation)
     * 表示拦截含有com.cxh.study.aop.controller.UserAccessAnnotation这个注解的类中所有方法
     *
     * @annotation(com.cxh.study.aop.controller.UserAccessAnnotation)
     * 表示拦截含有这个注解的方法
     */
    @Pointcut("@annotation(com.spring_JWT.annotation.MyLogAnnotation)")
    public void myLogPointCut() {

    }

    @Before("myLogPointCut()")
    public void beforeMyLogPointCut(JoinPoint joinPoint) {

        /** 获取方法签名 */
        Signature signature = joinPoint.getSignature();
        log.info("【类方法】" + signature.getDeclaringTypeName() + "." + signature.getName());

        /** 获取参数值 */
        log.info("【参数】{}", joinPoint.getArgs());

        /** 转化为方法签名 */
        MethodSignature methodSignature = (MethodSignature) signature;
        /** 获取方法签名中的方法 */
        Method method = methodSignature.getMethod();
        /** 获取方法的我们需要的注释 */
        MyLogAnnotation annotation = method.getAnnotation(MyLogAnnotation.class);

        log.info("【注释名称】"  + annotation.annotationType().getName());

        log.info("【注释value值】" + annotation.value());

    }

}
