package com.spring_JWT.exception;

import com.spring_JWT.enums.ResultEnum;
import lombok.Data;

/**
 * @Author Goddy
 * @Date Create in 下午3:51 2018/1/10
 */
@Data
public class MyException extends RuntimeException {

    private Integer code;

    public MyException(ResultEnum resultEnum) {

        super(resultEnum.getMsg());

        this.code = resultEnum.getCode();
    }

}
