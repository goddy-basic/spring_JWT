package com.spring_JWT.handle;

import com.spring_JWT.domain.entity.Result;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import com.spring_JWT.utils.ResultUtil;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author Goddy
 * @Date Create in 下午4:15 2018/1/11
 */
@ControllerAdvice
public class ExceptionHandle {

    /**
     * 捕获异常
     * 需要返回Json，上面没注解RestController
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e) {
        if (e instanceof MyException) {
            MyException myException = (MyException) e;
            return ResultUtil.error(myException.getCode(), e.getMessage());
        } else if (e instanceof MissingServletRequestParameterException) {
            return ResultUtil.error(ResultEnum.MISSING_REQUEST_PARAM_ERROR);
        } else {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
        }
    }

}
