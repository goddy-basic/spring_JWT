package com.spring_JWT.service;

import com.spring_JWT.domain.table.User;

import java.util.concurrent.TimeUnit;

/**
 * @Author Goddy
 * @Date Create in 下午6:24 2018/1/12
 */
public interface RedisService {

    /** 通过key查找 */
    Object findOne(String key);

    /** 保存 */
    void save(String key, Object value);
    void save(String key, Object value, long time, TimeUnit timeUnit);

    /** 用户保存，返回jwt */
    String save(User user, long time) throws Exception;

    /** 删除 */
    void delete(String key);

}
