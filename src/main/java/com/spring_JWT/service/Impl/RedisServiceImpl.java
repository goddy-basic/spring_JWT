package com.spring_JWT.service.Impl;

import com.spring_JWT.domain.table.User;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import com.spring_JWT.service.RedisService;
import com.spring_JWT.service.UserService;
import com.spring_JWT.utils.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @Author Goddy
 * @Date Create in 下午6:17 2018/1/12
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private UserService userService;

    @Override
    public Object findOne(String key) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        if (!redisTemplate.hasKey(key)) {
            throw new MyException(ResultEnum.REDIS_NO_KEY_ERROR);
        }
        return operations.get(key);
    }

    @Override
    public void save(String key, Object value) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value);
    }

    @Override
    public void save(String key, Object value, long time, TimeUnit timeUnit) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        operations.set(key, value, time, timeUnit);
    }

    @Override
    public String save(User user, long time) throws Exception {
        //1。生成JWT
        String jwt = JWTUtil.createJWT(user.getUid(), user.getUsername(), time);

        //2. 拼装格式
        HashMap<String, Object> map = new HashMap<>();
        map.put("uid", user.getUid());
        map.put("username", user.getUsername());
        map.put("role", userService.findRoles(user.getUid()));
        map.put("permission", userService.findPermissions(user.getUid()));

        //3。存储
        save(jwt, map, time, TimeUnit.MILLISECONDS);

        return jwt;
    }

    @Override
    public void delete(String key) {
        redisTemplate.delete(key);
    }

}
