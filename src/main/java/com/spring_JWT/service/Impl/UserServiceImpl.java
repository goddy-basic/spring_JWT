package com.spring_JWT.service.Impl;

import com.spring_JWT.domain.table.User;
import com.spring_JWT.domain.table.UserPermission;
import com.spring_JWT.domain.table.UserRole;
import com.spring_JWT.enums.ResultEnum;
import com.spring_JWT.exception.MyException;
import com.spring_JWT.repository.UserPermissionRepository;
import com.spring_JWT.repository.UserRepository;
import com.spring_JWT.repository.UserRoleRepository;
import com.spring_JWT.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author Goddy
 * @Date Create in 下午5:03 2018/1/10
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserPermissionRepository userPermissionRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public User saveUser(User user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new MyException(ResultEnum.DATABASE_SAVE_ERROR);
        }
    }

    @Override
    public void deleteUser(String uid) {
        //1、删除相关依赖
        userPermissionRepository.deleteByUid(uid);
        userRoleRepository.deleteByUid(uid);
        //2、删除
        userRepository.deleteByUid(uid);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findUser(String uid) {
        return userRepository.findByUid(uid);
    }

    @Override
    public User findUserByName(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void addRoles(String uid, String... roles) {
        if (roles == null || roles.length == 0) {
            return;
        }
        Set<String> roleSet = findRoles(uid);
        for (String role : roles) {
            if (!roleSet.contains(role)) {
                UserRole userRole = new UserRole();
                userRole.setUid(uid);
                userRole.setRole(role);
                userRoleRepository.save(userRole);
            }
        }
    }

    @Override
    public void deleteRoles(String uid, String... roles) {
        if (roles == null || roles.length == 0) {
            return;
        }
        for (String role : roles) {
            userRoleRepository.deleteByUidAndRole(uid, role);
        }
    }

    @Override
    public Set<String> findRoles(String uid) {
        Set<UserRole> userRoleSet = userRoleRepository.findByUid(uid);
        Set<String> result = userRoleSet.stream().map(e ->
            e.getRole()
        ).collect(Collectors.toSet());
        return result;
    }

    @Override
    public void addPermissions(String uid, String... permissions) {
        if (permissions == null || permissions.length == 0) {
            return;
        }
        Set<String> permissionSet = findPermissions(uid);
        for (String permission : permissions) {
            if (!permissionSet.contains(permission)) {
                UserPermission userPermission = new UserPermission();
                userPermission.setUid(uid);
                userPermission.setPermission(permission);
                userPermissionRepository.save(userPermission);
            }
        }
    }

    @Override
    public void deletePermissions(String uid, String... permissions) {
        if (permissions == null || permissions.length == 0) {
            return;
        }
        for (String permission : permissions) {
            userPermissionRepository.deleteByUidAndPermission(uid, permission);
        }
    }

    @Override
    public Set<String> findPermissions(String uid) {
        return userPermissionRepository.findByUid(uid)
                .stream().map(e ->
                        e.getPermission()
                ).collect(Collectors.toSet());
    }
}
