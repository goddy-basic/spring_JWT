package com.spring_JWT.service;

import com.spring_JWT.domain.table.User;
import com.spring_JWT.domain.table.UserPermission;
import com.spring_JWT.domain.table.UserRole;

import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午2:52 2018/1/10
 */
public interface UserService {

    /** 添加用户 */
    User saveUser(User user);

    /** 删除用户 */
    void deleteUser(String uid);

    /** 更新用户 */
    User updateUser(User user);

    /** 查找用户 */
    User findUser(String uid);

    /** 按用户名查找 */
    User findUserByName(String username);



    /** 添加角色 */
    void addRoles(String uid, String... roles);

    /** 删除角色 */
    void deleteRoles(String uid, String... roles);

    /** 通过uid查找角色 */
    Set<String> findRoles(String uid);



    /** 添加权限 */
    void addPermissions(String uid, String... permissions);

    /** 删除权限 */
    void deletePermissions(String uid, String... permissions);

    /** 通过uid查找权限 */
    Set<String> findPermissions(String uid);

}
