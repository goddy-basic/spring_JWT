package com.spring_JWT.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解
 * @Author Goddy
 * @Date Create in 下午8:53 2018/1/9
 *
 * @Target 说明了Annotation所修饰的对象范围
    取值(ElementType)有：
　　　　1.CONSTRUCTOR:用于描述构造器
　　　　2.FIELD:用于描述域
　　　　3.LOCAL_VARIABLE:用于描述局部变量
　　　　4.METHOD:用于描述方法
　　　　5.PACKAGE:用于描述包
　　　　6.PARAMETER:用于描述参数
　　　　7.TYPE:用于描述类、接口(包括注解类型) 或enum声明
 @Target(ElementType.TYPE)   //接口、类、枚举、注解
 @Target(ElementType.FIELD) //字段、枚举的常量
 @Target(ElementType.METHOD) //方法
 @Target(ElementType.PARAMETER) //方法参数
 @Target(ElementType.CONSTRUCTOR)  //构造函数
 @Target(ElementType.LOCAL_VARIABLE)//局部变量
 @Target(ElementType.ANNOTATION_TYPE)//注解
 @Target(ElementType.PACKAGE) ///包

 @Documented 用于描述其它类型的annotation应该被作为被标注的程序成员的公共API，
     因此可以被例如javadoc此类的工具文档化。
     Documented是一个标记注解，没有成员。
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyLogAnnotation {

    String value();

}
