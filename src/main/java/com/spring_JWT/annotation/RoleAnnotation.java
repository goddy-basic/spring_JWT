package com.spring_JWT.annotation;

import java.lang.annotation.*;

/**
 * @Author Goddy
 * @Date Create in 下午6:01 2018/1/10
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RoleAnnotation {

    String role();

}
