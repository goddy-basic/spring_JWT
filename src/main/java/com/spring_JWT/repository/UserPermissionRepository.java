package com.spring_JWT.repository;

import com.spring_JWT.domain.table.UserPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午3:27 2018/1/10
 */
public interface UserPermissionRepository extends JpaRepository<UserPermission, Long> {

    Set<UserPermission> findByUid(String uid);

    @Transactional
    void deleteByUid(String uid);

    @Transactional
    void deleteByUidAndPermission(String uid, String permission);
}
