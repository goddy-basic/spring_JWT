package com.spring_JWT.repository;

import com.spring_JWT.domain.table.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Goddy
 * @Date Create in 下午6:41 2018/1/9
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByUid(String uid);

    User findByUsername(String username);

    @Transactional
    void deleteByUsername(String username);

    @Transactional
    void deleteByUid(String uid);

}
