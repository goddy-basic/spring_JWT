package com.spring_JWT.repository;

import com.spring_JWT.domain.table.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @Author Goddy
 * @Date Create in 下午3:28 2018/1/10
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    Set<UserRole> findByUid(String uid);

    @Transactional
    void deleteByUid(String uid);

    @Transactional
    void deleteByUidAndRole(String uid, String role);

}
