package com.spring_JWT.enums;

import lombok.Getter;

/**
 * @Author Goddy
 * @Date Create in 下午3:58 2018/1/10
 */
@Getter
public enum ResultEnum {

    UNKNOWN_ERROR(-1, "未知错误"),

    SUCCESS(000, "返回正常"),

    DATABASE_SAVE_ERROR(101, "数据库存储失败"),

    DATABASE_NO_USER_ERROR(102, "数据库不存在该用户"),

    TOKEN_EXPIRED_ERROR(202, "token过期"),

    TOKEN_SIGNATURE_ERROR(203, "token签名失败"),

    TOKEN_OTHER_ERROR(204, "token其他错误"),

    UNAUTHORIZED_ERROR(305, "没访问权限"),

    REDIS_NO_KEY_ERROR(306, "redis不包含key"),

    MISSING_REQUEST_PARAM_ERROR(307, "缺少参数"),

    USER_WRONG_PASSWORD_ERROR(401, "输入密码错误"),

    ;


    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
